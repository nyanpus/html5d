# html5d

**NOTE** This is work-in-progress and very alpha stage.

html5d is an experimental HTML parser.

## Getting started

### Prerequirements

- D Langauge >= 2.066.0

### Tokenizer

We can run tests

```
$ rdmd -debug=html5d -unittest -main tokenizer/tokenizer.d
```
