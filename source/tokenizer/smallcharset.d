module tokenizer.smallcharset;

import std.conv : to;


struct SmallCharSet
{
  ulong bits;

  this(char[] set)
  {
    foreach (c; set) {
      bits |= 1 << c.to!uint;
    }
  }

  bool contains(ubyte n)
  {
    return 0 != (bits & (1 << n.to!ulong));
  }

  int nonmember_prefix_len(string buf)
  {
    int n = 0;
    foreach (b; buf) {
      if (b.to!ubyte >= 64 || !contains(b.to!ubyte)) {
        ++n;
      }
      else {
        break;
      }
    }
    return n;
  }
}


unittest
{
  import std.range : take, repeat;
  import std.array : join;

  foreach (c; ['&', '\0']) {
    foreach (ushort x; 0..48) {
      foreach (ushort y; 0..48) {
        string s = "x".repeat.take(x).join("") ~ c ~  "x".repeat.take(y).join("");
        auto set = new SmallCharSet(['&', '\0']);

        assert(x == set.nonmember_prefix_len(s));
      }
    }
  }
}
