module tokenizer.state;


enum State {
  Data,
  PlainText,
  TagOpen,
  EndTagOpen,
  TagName,
  BeforeAttributeName,
  SelfClosingStartTag,
  MarkupDeclarationOpen,
}








