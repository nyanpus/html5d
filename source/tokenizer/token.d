module tokenizer.token;


// Doctype token.
struct Doctype
{
  string name;
  string public_id;
  string system_id;
  bool force_quirks;
}


// Tag attribute
struct Attribute
{
  string name;
  string value;
}


enum TagKind {
  StartTag,
  EndTag,
}


struct Tag
{
  TagKind king;
  string name;
  bool self_closing;
  Attribute[] attrs;
}


enum TokenTypeID {
  DoctypeToken,
  TagToken,
  CommentToken,
  CharacterTokens,
  NullCharacterToken,
  EOFToken,
  ParseError,
}

struct Token
{
  TokenTypeID id;
  string str;

  this(TokenTypeID _id, string _str)
  {
    id = _id;
    str = _str;
  }
}


interface TokenSink
{
  void process_token(Token* token);
}
