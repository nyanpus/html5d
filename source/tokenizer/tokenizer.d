module tokenizer.tokenizer;

import std.conv;
import std.typecons;
import std.ascii;
import std.string;
import core.exception;

import tokenizer.smallcharset;
import tokenizer.bufferedqueue;
import tokenizer.token;
import tokenizer.state;


debug(html5d) import std.stdio;


class Tokenizer
{
private:
  TokenSink sink;  // Destination for tokens to emit.
  BufferedQueue input_buffers;
  State state;
  bool ignore_lf;
  char current_char;
  bool reconsume;
  string current_tag_name;
  bool current_tag_self_closing;
  Attribute[] current_tag_attrs;
  TagKind current_tag_kind;
  string current_attr_name;
  string last_start_tag_name;

public:

  this(TokenSink _sink)
  {
    sink = _sink;
    input_buffers = new BufferedQueue;
    state = State.Data;
    ignore_lf = false;
    current_char = '\0';
    reconsume = false;
    current_tag_name = "";
    current_tag_self_closing = false;
    current_tag_attrs = [];
    current_tag_kind = TagKind.StartTag;
    current_attr_name = "";
    last_start_tag_name = "";
  }

  void feed(string input)
  {
    if (input.length == 0) {
      return;
    }

    input_buffers ~= new Buffer(input);

    while ( step() ) {
    }    
  }

  string get_char()
  {
    if (reconsume) {
      reconsume = false;
      return current_char.to!string;
    }
    else {
      string s = input_buffers.next;
      return get_preprocessor_char(s);
    }
  }

  // void consume_char_ref()
  // {
  // }

  void emit_char(string c)
  {
    if (c == "\0") {
      process_token(new Token(TokenTypeID.NullCharacterToken, c));
    }
    process_token(new Token(TokenTypeID.CharacterTokens, c));
  }

  void emit_chars(string chars)
  {
    process_token(new Token(TokenTypeID.CharacterTokens, chars));
  }

  void process_token(Token* token)
  {
    sink.process_token(token);
  }

  string get_preprocessor_char(ref string c)
  {
    if ( ignore_lf ) {
      ignore_lf = false;
      if (c == "\n") {
        c = input_buffers.next();
      }
    }

    if (c == "\r") {
      ignore_lf = true;
      c = "\n";
    }

    debug(html5d) writeln("got charactor: ", c);

    current_char = c[0];

    return c;
  }

  void finish_attribute()
  {
    if (current_attr_name.length == 0) {
      return;
    }

    // FIXME: check for attribute duplication.
  }

  void emit_current_tag()
  {
    finish_attribute();

    final switch (current_tag_kind) {
    case TagKind.StartTag:
    {
      last_start_tag_name = current_tag_name;
    }
    case TagKind.EndTag:
    {
      if (current_tag_attrs.length != 0) {
        emit_error("Attributes on an end tag");
      }
      if (current_tag_self_closing) {
        emit_error("Self-Closing end tag");
      }
    }
    }
    process_token(new Token(TokenTypeID.TagToken, current_tag_name));
  }

  void discard_tag()
  {
    current_tag_name = "";
    current_tag_self_closing = false;
    current_tag_attrs = [];
  }

  void create_tag(TagKind kind, char c)
  {
    discard_tag();
    current_tag_name ~= c;
    current_tag_kind = kind;
  }

  void emit_error(string error)
  {
    process_token(new Token(TokenTypeID.ParseError, error));
  }

  void bad_char_error()
  {
    string msg = "#<Bad Character> Saw " ~ current_char ~ " in state: " ~ state.to!string;
    emit_error(msg);
  }

  Tuple!(string, bool) pop_except_from(SmallCharSet* set)
  {
    Tuple!(string, bool) result = input_buffers.pop_except_from(set);

    debug(html5d) writeln("got charactors: " ~ result[0]);

    if (result[1]) {
      get_preprocessor_char(result[0]);
    }

    return result;
  }

  bool step()
  {
    debug(html5d) writeln("processing in state: ", state);

    switch (state) {
    case State.Data:  // data-state
    {
      for (;;) {
        // (chr, is_set)
        Tuple!(string, bool) pair = pop_except_from(new SmallCharSet(['\r', '\0', '&', '<']));
        auto chars = pair[0];
        auto is_set = pair[1];

        if (is_set) {
          switch (chars) {
          case "<":
          {
            state = State.TagOpen;
            return true;
          }

          case "&":
          {
            throw new Exception("Not implemented yet");   // FIXME
            // consume_char_ref();
          }

          case "\0":
          {
            throw new Exception("Not implemented yet");   // FIXME
          }

          default:
          {
            emit_char(chars);
          }

          }
        }
        else {
          if (chars == "\0") {
            return false;
          }
          emit_chars(chars);
        }
      }
    }

    case State.PlainText:  // plaintext-state
    {
      throw new Exception("Not implemented yet");   // FIXME
    }

    case State.TagOpen:  // tag-open
    {
      for (;;) {
        string c;
        try {
          c = get_char();
        }
        catch (RangeError) {
          return false;
        }

        switch (c) {
        case "!":
        {
          state = State.MarkupDeclarationOpen;
          return true;
        }

        case "/":
        {
          state = State.EndTagOpen;
          return true;
        }

        default:
        {
          if (c[0].isLower) {
            create_tag(TagKind.StartTag, c[0]);
            state = State.TagName;
            return true;
          }
          else {
            bad_char_error();
            emit_char("<");
            reconsume = true;
            state = State.Data;
            return true;
          }
        }

        }
      }
    }

    case State.EndTagOpen:
    {
      for (;;) {
        string c;
        try {
          c = get_char();
        }
        catch (RangeError) {
          return false;
        }

        switch (c) {
        case ">":
        {
            bad_char_error();
            state = State.Data;
            return true;
        }

        default:
        {
          if (c[0].isLower) {
            create_tag(TagKind.EndTag, c[0]);
            state = State.TagName;
            return true;
          }
          else {
            throw new Exception("Not implemented yet!");  // FIXME
          }
        }

        }
      }
    }

    case State.TagName:
    {
      for (;;) {
        string c;
        try {
          c = get_char();
        }
        catch (RangeError) {
          return false;
        }

        switch (c) {
        case "\t":
        case "\n":
        case "\x0C":
        case " ":
        {
          state = State.BeforeAttributeName;
          return true;
        }

        case "/":
        {
          state = State.SelfClosingStartTag;
          return true;
        }

        case ">":
        {
          state = State.Data;
          emit_current_tag();
          return true;
        }

        default:
        {
          current_tag_name ~= c.toLower;
        }

        }
      }
    }

    case State.MarkupDeclarationOpen:
    {
      throw new Exception("Not implemented yet");   // FIXME
    }

    default:
      assert(false);
    }
  }
}


void tokenize_to(TokenSink sink, string[] input)
{
  auto tok = new Tokenizer(sink);

  foreach (s; input) {
    tok.feed(s);
  }
}


unittest
{
  class TestSink : TokenSink
  {
    void process_token(Token* token)
    {
      writeln("<Sink token> TokenTypeID:" ~ token.id.to!string ~  " str:"  ~ token.str);
    }
  }
  
  auto sink = new TestSink;

  auto inputs = ["<html></html>"];
  tokenize_to(sink, inputs);
}
