module tokenizer.bufferedqueue;

import std.typecons;
import core.exception;
import std.ascii;
import std.algorithm;
import std.range;

import tokenizer.smallcharset;


struct Buffer
{
  uint pos;
  string buf;

  this(string _buf, uint _pos = 0)
  {
    pos = _pos;
    buf = _buf;
  }

  ulong length() @property
  {
    return buf.length;
  }
  

  auto front() @property
  {
    return buf[0];
  }

  string opIndex(size_t x, size_t y)
  {
    return buf[x..y];
  }

  int opDollar(size_t _)()
  {
    return buf.length;
  }
}


class BufferedQueue
{
  Buffer*[] buffers;

  this()
  {
  }

  string front() @property
  {
    Buffer* buf = buffers[0];
    return  buf.buf[buf.pos..buf.pos+1];  // 1-length string
  }

  bool empty() @property
  {
    Buffer* buf = buffers[0];

    return buf.pos <= buf.length;
  }

  void popFront() @property
  {
    buffers = buffers[1..$];
  }

  ulong length() @property
  {
    return reduce!"a + b.length"(0L, buffers);
  }

  void pushFront(Buffer* buf) @property
  {
    if (buf.buf.length == 0) {
      return;
    }
    buffers = buf ~ buffers;
  }

  Buffer* opIndex(int v)
  {
    return buffers[v];  // May throw RangeError
  }
  
  void concatAssign(Buffer* buf)
  {
    buffers ~= buf;
  }

  template opOpAssign(string op) if (op == "~")
  {
    alias concatAssign opOpAssign;
  }

  string peek()
  {
    Buffer* buf = buffers[0];
    return buf.buf[buf.pos..buf.pos+1];
  }

  string next()
  {
    Buffer* buf = buffers[0];
    string ch = this.front;

    if (++buf.pos >= buf.length) {
      this.popFront();
    }

    return ch;
  }

  Tuple!(string, bool) pop_except_from(SmallCharSet* set)
  {
    bool now_empty;
    Tuple!(string, bool) result;

    Buffer* buf;

    try {
      buf = buffers[0];
    }
    catch (RangeError) {
      return tuple("\0", false);
    }

    int n = set.nonmember_prefix_len(buf.buf[buf.pos..$]);

    if (n > 0) {
      uint new_pos = buf.pos + n;
      string s = buf.buf[buf.pos..new_pos].dup;
      buf.pos = new_pos;
      result = tuple(s, false);
    }
    else {
      string ch = buf.buf[buf.pos..buf.pos+1].dup;  // 1-length string
      buf.pos += 1;
      result = tuple(ch, true);
    }

    if (!empty) {
      this.popFront();
    }

    return result;
  }

  bool eat(string pat)
  {
    uint buffers_exhausted = 0;
    int consumed_from_last = buffers[0].pos;

    foreach (c; pat) {
      if (buffers_exhausted >= buffers.length) {
        throw new RangeError;
      }

      Buffer* buf = buffers[buffers_exhausted];
      char d = buf.buf[consumed_from_last];

      if (c.isASCII && d.isASCII) {
        if (c != d) {
          return false;
        }
      }
      else {
        return false;
      }

      consumed_from_last++;
      if (consumed_from_last >= buf.length) {
        buffers_exhausted++;
        consumed_from_last = 0;
      }
    }
    foreach (_; 0..buffers_exhausted) {
      this.popFront();
    }

    Buffer* buf = buffers[0];
    buf.pos = consumed_from_last;

    return true;
  }
}


// smoke test
unittest
{
  import std.exception;

  scope bq = new BufferedQueue;
  assertThrown!RangeError(bq.peek());

  bq ~= new Buffer("abc");
  assert(bq.peek() == "a");
  assert(bq.next() == "a");
  assert(bq.peek() == "b");
  assert(bq.next() == "b");
  assert(bq.peek() == "c");
  assert(bq.next() == "c");
  assertThrown!RangeError(bq.peek());
  assertThrown!RangeError(bq.next());
}


// unconsume
unittest
{
  import std.exception;

  scope bq = new BufferedQueue;
  bq ~= new Buffer("abc");
  assert(bq.next() == "a");

  bq.pushFront(new Buffer("xy"));
  assert(bq.next() == "x");
  assert(bq.next() == "y");
  assert(bq.next() == "b");
  assert(bq.next() == "c");
  assertThrown!RangeError(bq.next());
}


// pop except set
unittest
{
  import std.exception;

  scope bq = new BufferedQueue;
  bq ~= new Buffer("abc&def");
  auto pop_func = () { return bq.pop_except_from(new SmallCharSet(['&'])); };
  assert(pop_func() == tuple("abc", false));
  assert(pop_func() == tuple("&", true));
  assert(pop_func() == tuple("def", false));
  assertThrown!RangeError(bq.next());
}


// push truncated
unittest
{
  import std.exception;

  scope bq = new BufferedQueue;
  bq ~= new Buffer("abc", 1);
  assert(bq.next() == "b");
  assert(bq.next() == "c");
  assertThrown!RangeError(bq.next());
}


// eat
unittest
{
  import std.exception;

  scope bq = new BufferedQueue;
  bq ~= new Buffer("a");
  bq ~= new Buffer("bc");

  assert(!bq.eat("ax"));
  assert(bq.eat("ab"));
  assert(bq.next() == "c");
}


// range interface
unittest
{
  import std.stdio;
  import std.exception;

  scope bq = new BufferedQueue;
  bq ~= new Buffer("a");
  bq ~= new Buffer("bc");

  foreach (buf; bq) {
  }
}